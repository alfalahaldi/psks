package client.android.psks.model.image;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/30/17.
 */

public class ImageResponse extends BaseResponse{

    private String originalFileName;

    private String contentType;

    private String binaryData;

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getBinaryData() {
        return binaryData;
    }

    public void setBinaryData(String binaryData) {
        this.binaryData = binaryData;
    }

    @Override
    public String toString() {
        return "ImageResponse{" +
                "originalFileName='" + originalFileName + '\'' +
                ", contentType='" + contentType + '\'' +
                ", binaryData='" + binaryData + '\'' +
                '}';
    }
}
