package client.android.psks.model.member;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/15/17.
 */

public class PKSMemberResponse extends BaseResponse{

    private Entity entity;

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    @Override
    public String toString() {
        return "PKSMemberResponse{" +
                "entity=" + entity +
                '}';
    }
}
