package client.android.psks.model.survey;

import java.io.Serializable;

/**
 * Created by winzaldi on 11/15/17.
 */

public class List  implements Serializable{

    private String countTQuestionnaireAssignment;

    private Questionnaire questionnaire;

    private SurveyAssignment surveyAssignment;

    private SurveySchedule surveySchedule;

    public String getCountTQuestionnaireAssignment() {
        return countTQuestionnaireAssignment;
    }

    public void setCountTQuestionnaireAssignment(String countTQuestionnaireAssignment) {
        this.countTQuestionnaireAssignment = countTQuestionnaireAssignment;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public SurveyAssignment getSurveyAssignment() {
        return surveyAssignment;
    }

    public void setSurveyAssignment(SurveyAssignment surveyAssignment) {
        this.surveyAssignment = surveyAssignment;
    }

    public SurveySchedule getSurveySchedule() {
        return surveySchedule;
    }

    public void setSurveySchedule(SurveySchedule surveySchedule) {
        this.surveySchedule = surveySchedule;
    }
}
