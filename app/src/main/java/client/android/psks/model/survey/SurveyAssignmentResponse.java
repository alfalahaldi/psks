package client.android.psks.model.survey;

import java.io.Serializable;
import java.util.Arrays;

import client.android.psks.model.response.BaseResponse;

/**
 * Created by winzaldi on 11/15/17.
 */

public class SurveyAssignmentResponse extends BaseResponse implements Serializable {


    private List[] list;

    public List[] getList() {
        return list;
    }

    public void setList(List[] list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "SurveyAssignmentResponse{" +
                "list=" + Arrays.toString(list) +
                '}';
    }
}
