package client.android.psks.model.survey;

import java.io.Serializable;

/**
 * Created by winzaldi on 11/15/17.
 */

public class SurveySchedule implements Serializable{

    private String questionnaireId;

    private String surveyProgramCode;

    private String scheduleFromDate;

    private String status;

    private String provinceCode;

    private String scheduleToDate;

    private String respondenTarget;

    private String provinceName;

    private String surveyScheduleName;

    private String surveyScheduleId;

    public String getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(String questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public String getSurveyProgramCode() {
        return surveyProgramCode;
    }

    public void setSurveyProgramCode(String surveyProgramCode) {
        this.surveyProgramCode = surveyProgramCode;
    }

    public String getScheduleFromDate() {
        return scheduleFromDate;
    }

    public void setScheduleFromDate(String scheduleFromDate) {
        this.scheduleFromDate = scheduleFromDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getScheduleToDate() {
        return scheduleToDate;
    }

    public void setScheduleToDate(String scheduleToDate) {
        this.scheduleToDate = scheduleToDate;
    }

    public String getRespondenTarget() {
        return respondenTarget;
    }

    public void setRespondenTarget(String respondenTarget) {
        this.respondenTarget = respondenTarget;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getSurveyScheduleName() {
        return surveyScheduleName;
    }

    public void setSurveyScheduleName(String surveyScheduleName) {
        this.surveyScheduleName = surveyScheduleName;
    }

    public String getSurveyScheduleId() {
        return surveyScheduleId;
    }

    public void setSurveyScheduleId(String surveyScheduleId) {
        this.surveyScheduleId = surveyScheduleId;
    }


    @Override
    public String toString() {
        return "SurveySchedule{" +
                "questionnaireId='" + questionnaireId + '\'' +
                ", surveyProgramCode='" + surveyProgramCode + '\'' +
                ", scheduleFromDate='" + scheduleFromDate + '\'' +
                ", status='" + status + '\'' +
                ", provinceCode='" + provinceCode + '\'' +
                ", scheduleToDate='" + scheduleToDate + '\'' +
                ", respondenTarget='" + respondenTarget + '\'' +
                ", provinceName='" + provinceName + '\'' +
                ", surveyScheduleName='" + surveyScheduleName + '\'' +
                ", surveyScheduleId='" + surveyScheduleId + '\'' +
                '}';
    }
}
