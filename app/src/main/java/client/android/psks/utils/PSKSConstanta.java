package client.android.psks.utils;

/**
 * Created by winzaldi on 11/16/17.
 */

public class PSKSConstanta {
    public static final String NEKOT_APP = "NEKOT_PPA";
    public static final String LOGIN_OBJ = "LOGIN_OBJ";
    public static final String SURVEY_ASSIGMENT_ID = "ASSIGNMENT_ID";
    public static final String DF_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DF_DD_MM_YYYY = "dd-MM-yyyy";
    public static final String LIST_SURVEY = "SURVEY_LIST";
    //public static final String LIST_PARTICIPANT = "LOGIN_OBJ";
    public static final int REQUEST_PICK_FILE = 7;

    //for imageview
    public static class IMAGE {
        public static final int CLOUMN = 100;
        public static final int VALUE = 200;
    }

    //public static List<String> PSKS_TYPE = Arrays.asList("Perseorangan","Keluarga","Kelompok","Masyarakat");
    public static String PARTICIPANT_TYPE = "PARTICIPANT_TYPE";
    public static String IMG_PROFILE = "IMG_PROFILE";


    public static String MEDIA_IMAGE = "image/*";
    public static String MEDIA_VIDEO = "video/mp4";


}
