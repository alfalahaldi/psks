package client.android.psks.utils;

import com.google.gson.Gson;

import client.android.psks.model.district.DistrictResponse;
import client.android.psks.model.parameter.ParameterGroupResponse;
import client.android.psks.model.province.ProvinceResponse;
import client.android.psks.model.subdistrict.SubDistrictResponse;
import client.android.psks.model.village.VillageResponse;

/**
 * Created by winzaldi on 11/22/17.
 */

public class FakeReferences {

    public static final String PROP_JSON = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"provinceName\": \"BANGKA BELITUNG\",\n" +
            "            \"provinceCode\": \"BABEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"KALIMANTAN TENGAH\",\n" +
            "            \"provinceCode\": \"KALTENG\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SUMATERA SELATAN\",\n" +
            "            \"provinceCode\": \"SUMSEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"DAERAH ISTIMEWA YOGYAKARTA\",\n" +
            "            \"provinceCode\": \"DIY\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"JAMBI\",\n" +
            "            \"provinceCode\": \"JAMBI\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"MALUKU\",\n" +
            "            \"provinceCode\": \"MLK\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"RIAU\",\n" +
            "            \"provinceCode\": \"RIAU\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"JAKARTA\",\n" +
            "            \"provinceCode\": \"DKI\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"KEPULAUAN RIAU\",\n" +
            "            \"provinceCode\": \"KEPRI\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"KALIMANTAN TIMUR\",\n" +
            "            \"provinceCode\": \"KALTIM\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SUMATERA UTARA\",\n" +
            "            \"provinceCode\": \"SUMUT\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"PAPUA BARAT\",\n" +
            "            \"provinceCode\": \"PABAR\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"KALIMANTAN UTARA\",\n" +
            "            \"provinceCode\": \"KALUT\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"BENGKULU\",\n" +
            "            \"provinceCode\": \"BENGKULU\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SULAWESI UTARA\",\n" +
            "            \"provinceCode\": \"SULUT\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"LAMPUNG\",\n" +
            "            \"provinceCode\": \"LMPG\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"NUSA TENGGARA BARAT\",\n" +
            "            \"provinceCode\": \"NTB\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"NUSA TENGGARA TIMUR\",\n" +
            "            \"provinceCode\": \"NTT\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SULAWESI TENGGARA\",\n" +
            "            \"provinceCode\": \"SULTRA\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"JAWA BARAT\",\n" +
            "            \"provinceCode\": \"JABAR\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"KALIMANTAN SELATAN\",\n" +
            "            \"provinceCode\": \"KALSEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"MALUKU UTARA\",\n" +
            "            \"provinceCode\": \"MALUT\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SUMATERA BARAT\",\n" +
            "            \"provinceCode\": \"SUMBAR\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"JAWA TENGAH\",\n" +
            "            \"provinceCode\": \"JATENG\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SULAWESI SELATAN\",\n" +
            "            \"provinceCode\": \"SULSEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"GORONTALO\",\n" +
            "            \"provinceCode\": \"GORON\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"PAPUA\",\n" +
            "            \"provinceCode\": \"PAPUA\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"KALIMANTAN BARAT\",\n" +
            "            \"provinceCode\": \"KALBAR\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"JAWA TIMUR\",\n" +
            "            \"provinceCode\": \"JATIM\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"DAERAH ISTIMEWA ACEH\",\n" +
            "            \"provinceCode\": \"D.I.ACEH\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SULAWESI TENGAH\",\n" +
            "            \"provinceCode\": \"SULTEN\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"BALI\",\n" +
            "            \"provinceCode\": \"BALI\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"BANTEN\",\n" +
            "            \"provinceCode\": \"Banten\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"provinceName\": \"SULAWESI BARAT\",\n" +
            "            \"provinceCode\": \"SULBAR\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String DISTRICT_JSON = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"districtName\": \"KABUPATEN WONOGIRI\",\n" +
            "            \"isCity\": false,\n" +
            "            \"provinceCode\": \"JATENG\",\n" +
            "            \"districtCode\": \"KAB.WOGI\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"districtName\": \"KABUPATEN GOWA\",\n" +
            "            \"provinceCode\": \"SULSEL\",\n" +
            "            \"districtCode\": \"KAB.GOWA\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"districtName\": \"KABUPATEN SIGI\",\n" +
            "            \"provinceCode\": \"SULTEN\",\n" +
            "            \"districtCode\": \"KAB.SIGI\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String  SUBDISTRICT = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"districtCode\": \"KAB.MNBRT\",\n" +
            "            \"subDistrictName\": \"TIWORO SELATAN\",\n" +
            "            \"subDistrictCode\": \"TIWOSEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"districtCode\": \"KAB.BODIG\",\n" +
            "            \"subDistrictName\": \"SUBUR\",\n" +
            "            \"subDistrictCode\": \"SUBUR\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"districtCode\": \"KAB.KARO\",\n" +
            "            \"subDistrictName\": \"SIMPANG EMPAT\",\n" +
            "            \"subDistrictCode\": \"SIMPAEM\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"districtCode\": \"KAB.LADAU\",\n" +
            "            \"subDistrictName\": \"BULIK TIMUR\",\n" +
            "            \"subDistrictCode\": \"BULTIMU\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static final String VILL_JSON = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"isVillage\": true,\n" +
            "            \"villageName\": \"COT GIREK KANDANG\",\n" +
            "            \"zipCode\": \"24352\",\n" +
            "            \"villageCode\": \"CTGKG\",\n" +
            "            \"subDistrictCode\": \"TIWOSEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"isVillage\": true,\n" +
            "            \"villageName\": \"MEUNASAH MEE\",\n" +
            "            \"zipCode\": \"24352\",\n" +
            "            \"villageCode\": \"MNMEE\",\n" +
            "            \"subDistrictCode\": \"TIWOSEL\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"isVillage\": true,\n" +
            "            \"villageName\": \"Cot Ba'U\",\n" +
            "            \"zipCode\": \"23522\",\n" +
            "            \"villageCode\": \"Cot Ba'u\",\n" +
            "            \"subDistrictCode\": \"SUKAJAYA\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";
    
    private static final String JSON_PARTYCYPANT = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"seqNo\": 1,\n" +
            "            \"description\": \"PERSEORANGAN\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PARTICIPANT_TYPE\",\n" +
            "                \"valueCode\": \"1\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 4,\n" +
            "            \"description\": \"DUNIA USAHA\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PARTICIPANT_TYPE\",\n" +
            "                \"valueCode\": \"4\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 2,\n" +
            "            \"description\": \"KELUARGA\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PARTICIPANT_TYPE\",\n" +
            "                \"valueCode\": \"2\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 3,\n" +
            "            \"description\": \"KELEMBAGAAN\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PARTICIPANT_TYPE\",\n" +
            "                \"valueCode\": \"3\"\n" +
            "            }\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    private static final String PSKS_TYPE = "{\n" +
            "    \"entities\": [\n" +
            "        {\n" +
            "            \"seqNo\": 4,\n" +
            "            \"description\": \"PSP - Pekerja Sosial Profesional\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PART$1\",\n" +
            "                \"valueCode\": \"PSP\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 1,\n" +
            "            \"description\": \"PSM - Pekerja Sosial Masyarakat\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PART$1\",\n" +
            "                \"valueCode\": \"PSM\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 2,\n" +
            "            \"description\": \"TKSK - Tenaga Kesejahteraan Sosial Kecamatan\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PART$1\",\n" +
            "                \"valueCode\": \"TKSK\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 3,\n" +
            "            \"description\": \"TAGANA - Taruna Siaga Bencana\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PART$1\",\n" +
            "                \"valueCode\": \"TAGANA\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 5,\n" +
            "            \"description\": \"WPKS - Wanita Pemimpin Kesejahteraan Sosial\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PART$1\",\n" +
            "                \"valueCode\": \"WPKS\"\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"seqNo\": 6,\n" +
            "            \"description\": \"PS - Penyuluh Sosial\",\n" +
            "            \"mParameterGroupValuePK\": {\n" +
            "                \"groupCode\": \"PART$1\",\n" +
            "                \"valueCode\": \"PS\"\n" +
            "            }\n" +
            "        }\n" +
            "    ],\n" +
            "    \"responseCode\": \"00\",\n" +
            "    \"responseMessage\": \"Success\"\n" +
            "}";

    public static ParameterGroupResponse  getParticipantType(){
        Gson gson = new Gson();
        return gson.fromJson(JSON_PARTYCYPANT,  ParameterGroupResponse.class);
    }

    public static ParameterGroupResponse  getPSKSTYPEType(){
        Gson gson = new Gson();
        return gson.fromJson(PSKS_TYPE,  ParameterGroupResponse.class);
    }


    public static ProvinceResponse getProvinsi(){
        Gson gson = new Gson();
        return gson.fromJson(PROP_JSON,  ProvinceResponse.class);
    }

    public static DistrictResponse getDistrict(){
        Gson gson = new Gson();
        return gson.fromJson(DISTRICT_JSON,  DistrictResponse.class);
    }

    public static SubDistrictResponse getSubDistrict(){
        Gson gson = new Gson();
        return gson.fromJson(SUBDISTRICT,  SubDistrictResponse.class);
    }

    public static VillageResponse getVillage(){
        Gson gson = new Gson();
        return gson.fromJson(VILL_JSON,  VillageResponse.class);
    }


}
