package client.android.psks;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.HashMap;

import client.android.psks.model.quitioner.Entities;
import client.android.psks.utils.FakeUtils;
import client.android.psks.utils.Utils;

/**
 * Created by winzaldi on 11/25/17.
 */

public class FormStepperActivity  extends AppCompatActivity {
    //public class FormStepperActivity  extends AppCompatActivity implements StepperLayout.StepperListener, DataManager {

    private static final String TAG = FormStepperActivity.class.getSimpleName();
   // private StepperLayout mStepperLayout;
    private HashMap<Integer,Entities[]> hashMap = new HashMap<>();
    private HashMap<String,String> data = new HashMap<>();
    public static final String CURRENT_STEP_POSITION_KEY = "position";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mstepper);
       // mStepperLayout =  findViewById(R.id.stepperLayout);
        Log.i(TAG,"STEPPER");
        Entities[] entities = FakeUtils.getQuestioner(FakeUtils.FORM_JSON_2).getEntities();
        hashMap = Utils.splitQuestionEntities(entities);
        Log.i(TAG,"SIZES"+hashMap.size()+"");
        int startingStepPosition = 0;

        if (savedInstanceState != null) {
            startingStepPosition = savedInstanceState.getInt(CURRENT_STEP_POSITION_KEY);
            data  =  Utils.fromBundle(savedInstanceState);

        }
//        FormStepperAdapter stepperAdapter = new FormStepperAdapter(getSupportFragmentManager(),this,hashMap);
//        mStepperLayout.setAdapter(stepperAdapter,startingStepPosition);
//        mStepperLayout.setListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
//        outState.putInt(CURRENT_STEP_POSITION_KEY, mStepperLayout.getCurrentStepPosition());
//        outState.putAll(Utils.toBundle(data));
//        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        int currentStepPosition = mStepperLayout.getCurrentStepPosition();
//        if (currentStepPosition > 0) {
//            mStepperLayout.onBackClicked();
//        } else {
//            finish();
//        }
    }

//    @Override
//    public void saveData(String key, String value) {
//          data.put(key,value);
//    }
//
//    @Override
//    public HashMap<String,String> getData() {
//        return data;
//    }
//
//    @Override
//    public void onCompleted(View completeButton) {
//        Toast.makeText(this, "onCompleted! -> ", Toast.LENGTH_SHORT).show();
//    }

//    @Override
//    public void onError(VerificationError verificationError) {
//        Toast.makeText(this, "onErrro! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onStepSelected(int newStepPosition) {
//        Toast.makeText(this, "onStepSelected! -> " + newStepPosition, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onReturn() {
//        finish();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
