package client.android.psks.ui.member;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import client.android.psks.MainActivity;
import client.android.psks.R;
import client.android.psks.model.district.DistrictAdapter;
import client.android.psks.model.district.DistrictResponse;
import client.android.psks.model.image.ImageResponse;
import client.android.psks.model.login.LoginResponse;
import client.android.psks.model.member.PKSMemberResponse;
import client.android.psks.model.parameter.Entities;
import client.android.psks.model.parameter.ParameterGroupResponse;
import client.android.psks.model.province.ProvinceAdapter;
import client.android.psks.model.province.ProvinceResponse;
import client.android.psks.model.response.BaseResponse;
import client.android.psks.model.subdistrict.SubDistrictResponse;
import client.android.psks.model.subdistrict.SubdistrictAdapter;
import client.android.psks.model.village.VillageAdapter;
import client.android.psks.model.village.VillageResponse;
import client.android.psks.service.PSKSServices;
import client.android.psks.service.ServiceGenerator;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import client.android.psks.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by winzaldi on 11/29/17.
 */

public class FragmentMember extends Fragment implements MainActivity.OnBackPressedListener {
    private static final String TAG = FragmentMember.class.getSimpleName();
    private EditText et_member_code, et_pks_no, et_pks_name, et_member_addres, et_member_phone;
    private MaterialSpinner sp_pkstype, sp_participant_type, sp_coordinator, sp_status, sp_gender;
    private SearchableSpinner sp_province_code, sp_district_code, sp_subdistrict_code, sp_village_code;
    private CoordinatorLayout coordinatorLayout;
    private ImageView img_photo;
    private Button btn_update;
    private PSKSServices services = null;
    private LoginResponse user;
    private ImagePicker imagePicker;
    private ImageResponse imageResponse;
    private StringBuilder sb = null;
    private ProgressBar progressBar;
    private long mLastClickTime = 0;
    public static final String MSG_FAILED = "Gagal loading data ";

    public static FragmentMember newInstance() {
        return new FragmentMember();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        user = new Gson().fromJson(SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.LOGIN_OBJ, ""), LoginResponse.class);
        Log.i(TAG, user.toString());
        View view = inflater.inflate(R.layout.fragment_member, container, false);
        ((MainActivity) getActivity()).setOnBackPressedListener(this);
        progressBar = view.findViewById(R.id.progressBar);
        coordinatorLayout = view.findViewById(R.id.coordinator);
        String token = SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.NEKOT_APP, "");
        services = ServiceGenerator.createService(PSKSServices.class, getActivity(),
                token);

        btn_update = view.findViewById(R.id.btn_update);

        et_member_code = view.findViewById(R.id.et_member_code);
        et_pks_no = view.findViewById(R.id.et_pks_no);
        et_pks_name = view.findViewById(R.id.et_pks_name);
        et_member_addres = view.findViewById(R.id.et_member_addres);

        sp_coordinator = view.findViewById(R.id.sp_coordinator);
        String[] listCoord = getResources().getStringArray(R.array.arr_koordinator);
        final ArrayAdapter<String> coorAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, listCoord);
        sp_coordinator.setAdapter(coorAdapter);

        sp_status = view.findViewById(R.id.sp_status);
        String[] listStatus = getResources().getStringArray(R.array.arr_status);
        ArrayAdapter<String> statusAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, listStatus);
        sp_status.setAdapter(statusAdapter);

        sp_gender = view.findViewById(R.id.sp_gender);
        String[] listGender = getResources().getStringArray(R.array.jenis_kelamin);
        ArrayAdapter<String> genderAdapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, listGender);
        sp_gender.setAdapter(genderAdapter);


        img_photo = view.findViewById(R.id.img_photo);
        et_member_phone = view.findViewById(R.id.et_member_phone);

        sp_province_code = view.findViewById(R.id.sp_province_code);
        sp_province_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (sp_district_code != null) {
                    client.android.psks.model.province.Entities entity = (client.android.psks.model.province.Entities) sp_province_code.getSelectedItem();
                    progressBar.setVisibility(View.VISIBLE);
                    Call<DistrictResponse> calldistrict = services.getDistictByProvince(entity.getProvinceCode());
                    calldistrict.enqueue(new Callback<DistrictResponse>() {
                        @Override
                        public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            DistrictAdapter adapter =
                                    new DistrictAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                            sp_district_code.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<DistrictResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            //Log.i(TAG, "Failed load District");
                            Utils.showSnakbarMessage(view, "Gagal load data Kode Kabupaten/ Kota Madya");

                        }
                    });
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_district_code = view.findViewById(R.id.sp_district_code);
        sp_district_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (sp_subdistrict_code != null) {
                    client.android.psks.model.district.Entities entity = (client.android.psks.model.district.Entities) sp_district_code.getSelectedItem();
                    progressBar.setVisibility(View.VISIBLE);
                    Call<SubDistrictResponse> calldistrict = services.getSubDistictByDistrictCode(entity.getDistrictCode());
                    calldistrict.enqueue(new Callback<SubDistrictResponse>() {
                        @Override
                        public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            SubdistrictAdapter adapter =
                                    new SubdistrictAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                            sp_subdistrict_code.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            //Log.i(TAG, "Failed load District");
                            Utils.showSnakbarMessage(view, "Gagal load data Kode Kecamatan");

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_subdistrict_code = view.findViewById(R.id.sp_subdistrict_code);
        sp_subdistrict_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, final View view, int position, long id) {
                if (sp_village_code != null) {
                    client.android.psks.model.subdistrict.Entities entity = (client.android.psks.model.subdistrict.Entities) sp_subdistrict_code.getSelectedItem();
                    progressBar.setVisibility(View.VISIBLE);
                    Call<VillageResponse> calldistrict = services.getVillageBySubDistrictCode(entity.getSubDistrictCode());
                    calldistrict.enqueue(new Callback<VillageResponse>() {
                        @Override
                        public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {
                            progressBar.setVisibility(View.GONE);
                            VillageAdapter adapter =
                                    new VillageAdapter(getContext(), android.R.layout.simple_spinner_item, response.body().getEntities());
                            sp_village_code.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<VillageResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            //Log.i(TAG, "Failed load Village");
                            Utils.showSnakbarMessage(coordinatorLayout, "Gagal load data Kode Kelurahan/Desa");

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_village_code = view.findViewById(R.id.sp_village_code);

        sp_participant_type = view.findViewById(R.id.sp_participant_type);
        sp_pkstype = view.findViewById(R.id.sp_pkstype);

        services = ServiceGenerator.createService(PSKSServices.class, getContext(),
                SharedPrefsUtils.getFromPrefs(getActivity(), PSKSConstanta.NEKOT_APP, ""));

        Call<PKSMemberResponse> callmember = services.getPsksMemberDetails(user.getEntity().getPsksMemberId());

        callmember.enqueue(new Callback<PKSMemberResponse>() {
            @Override
            public void onResponse(Call<PKSMemberResponse> call, Response<PKSMemberResponse> response) {
                PKSMemberResponse res = response.body();
                if (response.code() == 200) {
                    setData(res);
                }
                //Log.i(TAG, res.getEntity().toString());

            }

            @Override
            public void onFailure(Call<PKSMemberResponse> call, Throwable t) {
                //Toast.makeText(getActivity(), "Data Member tidak ditemukan", Toast.LENGTH_SHORT).show();
                Utils.showSnakbarMessage(coordinatorLayout, "Data Member tidak ditemukan");
            }
        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                String memberId = et_member_code.getText().toString();
                Entities enGroup = (Entities) sp_pkstype.getItems().get(sp_pkstype.getSelectedIndex());
                String psksType = enGroup.getmParameterGroupValuePK().getValueCode();
                String psksNumber = et_pks_no.getText().toString();
                String psksName = et_pks_name.getText().toString();
                client.android.psks.model.province.Entities entProv =
                        (client.android.psks.model.province.Entities) sp_province_code.getSelectedItem();
                client.android.psks.model.district.Entities entDistr =
                        (client.android.psks.model.district.Entities) sp_district_code.getSelectedItem();
                client.android.psks.model.subdistrict.Entities entSubDistr =
                        (client.android.psks.model.subdistrict.Entities) sp_subdistrict_code.getSelectedItem();
                String provCode = entProv.getProvinceCode();
                String districtCode = entDistr.getDistrictCode();
                String subDistrictCode = entSubDistr.getSubDistrictCode();
                String villCode = "";
                if (sp_village_code.getSelectedItem() != null) {
                    client.android.psks.model.village.Entities entVillage =
                            (client.android.psks.model.village.Entities) sp_village_code.getSelectedItem();
                    villCode = entVillage.getVillageCode();
                }
                int intCoordinator = sp_coordinator.getText().toString().equalsIgnoreCase("YA") ? 1 : 0;
                String address = et_member_addres.getText().toString();
                String phone = et_member_phone.getText().toString();
                client.android.psks.model.parameter.Entities entPart =
                        (client.android.psks.model.parameter.Entities) sp_participant_type.getItems().get(sp_participant_type.getSelectedIndex());
                String participantType = entPart.getmParameterGroupValuePK().getValueCode();
                String photoId = user.getEntity().getPhotoId();

                String imageRequest;
                if (img_photo.getTag(R.id.value) == null) {
                    sb = createFromExistinct();
                } else {
                    sb = createImageStr(img_photo.getTag(R.id.value).toString());
                    //imageRequest = createImageRequest(img_photo.getTag(R.id.value).toString());
                }
                Call<BaseResponse> callMember = services.modifyPsksMemberEncode(memberId, psksType, psksNumber,
                        psksName, provCode, districtCode, subDistrictCode, villCode, intCoordinator, address, photoId,
                        phone, participantType, sb.toString());
                progressBar.setVisibility(View.VISIBLE);
                callMember.enqueue(new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        progressBar.setVisibility(View.GONE);
                        Log.i(TAG, response.message());

                        if (response.code() == 200) {
                            Utils.showSnakbarMessage(coordinatorLayout, "Success to Updated");
                        } else if (response.code() == 408) {
                            Utils.showSnakbarMessage(coordinatorLayout, "Request Time Out");
                        } else {
                            Utils.showSnakbarMessage(coordinatorLayout, "Failed to Updated");
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                        Log.i(TAG, t.getLocalizedMessage());
                        Utils.showSnakbarMessage(coordinatorLayout, "Failed to Updated");
                    }
                });

            }
        });

        img_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePicker = new ImagePicker.Builder(getActivity())
                        .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                        .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                        .directory(ImagePicker.Directory.DEFAULT)
                        .extension(ImagePicker.Extension.JPG)
                        .extension(ImagePicker.Extension.PNG)
                        .scale(600, 600)
                        .allowMultipleImages(false)
                        .enableDebuggingMode(true)
                        .build();
            }
        });

        /*
                //Fake Data
                PKSMemberResponse response = FakeUtils.getPKSMember();
                Log.i(TAG,response.getEntity().toString());
                setData(response);

        * */

//        btn_update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i(TAG, "Blm di implementasi");
//            }
//        });

        sp_participant_type.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
//                String participan = sp_participant_type.getText().toString().length() == 0 ? "1" : sp_participant_type.getText().toString();
//                String kodeParticipan = getParticipanType(getActivity(), participan);
                client.android.psks.model.parameter.Entities entPart =
                        (client.android.psks.model.parameter.Entities) sp_participant_type.getItems().get(sp_participant_type.getSelectedIndex());
                String param = "PART$" + entPart.getmParameterGroupValuePK().getValueCode();
                Log.i(TAG, "onItemSelected: " + param);
                Call<ParameterGroupResponse> callPsks = services.getPsksType(param);
                callPsks.enqueue(new Callback<ParameterGroupResponse>() {
                    @Override
                    public void onResponse(Call<ParameterGroupResponse> call, Response<ParameterGroupResponse> response) {
                        Entities[] list = response.body().getEntities();

                        Log.i(TAG, "onResponse: List" + list.length);
//                        ParameterGroupAdapter adapter =
//                                new ParameterGroupAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, list);

                        sp_pkstype.setItems(list);
                        if (list.length == 1) {
                            sp_pkstype.setItems(list[0].getDescription(), "");
                        }
                    }

                    @Override
                    public void onFailure(Call<ParameterGroupResponse> call, Throwable t) {
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Tipe Partisipan ");
                    }
                });
            }
        });

        return view;
    }

    private void setData(PKSMemberResponse res) {
        et_member_code.setText(res.getEntity().getPsksMemberId());
        et_pks_no.setText(res.getEntity().getPsksNo());
        et_pks_name.setText(res.getEntity().getPsksName());
        et_member_addres.setText(res.getEntity().getAddress());

        if (res.getEntity().getIsCoordinator().equalsIgnoreCase("true")) {
            sp_coordinator.setSelectedIndex(0);
        } else {
            sp_coordinator.setSelectedIndex(1);
        }

        if (res.getEntity().getStatus().equalsIgnoreCase("A")) {
            sp_status.setSelectedIndex(0);

        } else {
            sp_status.setSelectedIndex(1);
        }

        if (res.getEntity().getGender().equalsIgnoreCase("L")) {
            sp_gender.setSelectedIndex(0);
        } else {
            sp_gender.setSelectedIndex(1);
        }

        et_member_phone.setText(res.getEntity().getPhoneNumber());
        setParticipantType(res);
        setPsksType(res);
        setProvinceCode(res);
        setDistrictCode(res);
        setSubDistrictCode(res);
        setVillageCode(res);

        //String token = "816a884d-f588-4915-a8c1-2f379ff674a7";
        Call<ImageResponse> call = services.getBinaryData(res.getEntity().getPhotoId());
        call.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                if (response.code() == 200) {
                    Log.i(TAG, "image success download");
                    imageResponse = response.body();


                    //String decodeUrl = "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
                    byte[] decodedImage = Base64.decode(response.body().getBinaryData(), Base64.DEFAULT);
                    //byte[] decodedImage = Base64.decode(decodeUrl.getBytes(), Base64.DEFAULT);
                    Log.i(TAG, "PANJANG ::" + decodedImage.length);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
                    if (bitmap != null) {
                        img_photo.setImageBitmap(bitmap);
                        img_photo.setTag(response.body().getOriginalFileName());
                    } else {
                        Utils.showSnakbarMessage(coordinatorLayout, " Gagal menampilkan gambar ");
                        img_photo.setImageDrawable(getResources().getDrawable(R.drawable.ic_thumbnails));
                    }

                }
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {
                Log.i(TAG, "image Failed to download");
            }
        });


    }

    private void setVillageCode(final PKSMemberResponse res) {
        //VillageResponse resdata = FakeReferences.getVillage();

        if (sp_subdistrict_code.getSelectedItem() != null) {
            client.android.psks.model.subdistrict.Entities entity =
                    (client.android.psks.model.subdistrict.Entities) sp_subdistrict_code.getSelectedItem();
            if (entity != null) {

                Call<VillageResponse> callvillage = services.getVillageBySubDistrictCode(entity.getSubDistrictCode());
                callvillage.enqueue(new Callback<VillageResponse>() {
                    @Override
                    public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {

                        List<client.android.psks.model.village.Entities> list = Arrays.asList(response.body().getEntities());
                        ArrayAdapter<client.android.psks.model.village.Entities> adapter =
                                new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
                        sp_village_code.setAdapter(adapter);


                        int position = 0;
                        if (res.getEntity().getSubDistrictCode() != null) {

                            for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                client.android.psks.model.village.Entities e = response.body().getEntities()[idx];
                                if (res.getEntity().getVillageCode().equalsIgnoreCase(e.getVillageCode())) {
                                    position = idx;
                                }
                            }
                        }
                        sp_village_code.setSelection(position);

                    }

                    @Override
                    public void onFailure(Call<VillageResponse> call, Throwable t) {
                        //Log.i(TAG, "Faild Load Village ");
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Desa ");
                    }
                });

            }

        } else {

            Call<VillageResponse> callvillage = services.getAllVillage();
            callvillage.enqueue(new Callback<VillageResponse>() {
                @Override
                public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {

                    List<client.android.psks.model.village.Entities> list = Arrays.asList(response.body().getEntities());
                    ArrayAdapter<client.android.psks.model.village.Entities> adapter =
                            new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
                    sp_village_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getSubDistrictCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.village.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getVillageCode().equalsIgnoreCase(e.getVillageCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_village_code.setSelection(position);

                }

                @Override
                public void onFailure(Call<VillageResponse> call, Throwable t) {
                    //Log.i(TAG, "Faild Load Village ");
                    Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Desa ");
                }
            });

        }


    }

    private void setSubDistrictCode(final PKSMemberResponse res) {
        //SubDistrictResponse resdata = FakeReferences.getSubDistrict();

        if (sp_district_code.getSelectedItem() != null) {
            client.android.psks.model.district.Entities entitiy =
                    (client.android.psks.model.district.Entities) sp_district_code.getSelectedItem();
            if (entitiy != null) {
                Call<SubDistrictResponse> subDistrictcall = services.getSubDistictByDistrictCode(entitiy.getDistrictCode());
                subDistrictcall.enqueue(new Callback<SubDistrictResponse>() {
                    @Override
                    public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                        List<client.android.psks.model.subdistrict.Entities> list = Arrays.asList(response.body().getEntities());
                        ArrayAdapter<client.android.psks.model.subdistrict.Entities> adapter =
                                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
                        sp_subdistrict_code.setAdapter(adapter);

                        int position = 0;
                        if (res.getEntity().getSubDistrictCode() != null) {

                            for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                client.android.psks.model.subdistrict.Entities e = response.body().getEntities()[idx];
                                if (res.getEntity().getSubDistrictCode().equalsIgnoreCase(e.getSubDistrictCode())) {
                                    position = idx;
                                }
                            }
                        }
                        sp_subdistrict_code.setSelection(position);
                    }

                    @Override
                    public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                        //Log.i(TAG, "Faild Load SubDistrict Data ");
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Sub District ");

                    }
                });

            }

        } else {

            Call<SubDistrictResponse> subDistrictcall = services.getSubDistrict();
            subDistrictcall.enqueue(new Callback<SubDistrictResponse>() {
                @Override
                public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                    List<client.android.psks.model.subdistrict.Entities> list = Arrays.asList(response.body().getEntities());
                    ArrayAdapter<client.android.psks.model.subdistrict.Entities> adapter =
                            new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, list);
                    sp_subdistrict_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getSubDistrictCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.subdistrict.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getSubDistrictCode().equalsIgnoreCase(e.getSubDistrictCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_subdistrict_code.setSelection(position);
                }

                @Override
                public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                    //Log.i(TAG, "Faild Load SubDistrict Data ");
                    Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Sub District ");
                }
            });

        }


    }

    private void setDistrictCode(final PKSMemberResponse res) {

        //DistrictResponse resdata = FakeReferences.getDistrict();

        if (sp_province_code.getSelectedItem() != null) {
            client.android.psks.model.province.Entities entity =
                    (client.android.psks.model.province.Entities) sp_province_code.getSelectedItem();
            if (entity != null) {

                Call<DistrictResponse> calldistrict = services.getDistictByProvince(entity.getProvinceCode());
                calldistrict.enqueue(new Callback<DistrictResponse>() {
                    @Override
                    public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                        List<client.android.psks.model.district.Entities> list = Arrays.asList(response.body().getEntities());
                        ArrayAdapter<client.android.psks.model.district.Entities> adapter =
                                new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
                        sp_district_code.setAdapter(adapter);

                        int position = 0;
                        if (res.getEntity().getDistrictCode() != null) {

                            for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                client.android.psks.model.district.Entities e = response.body().getEntities()[idx];
                                if (res.getEntity().getDistrictCode().equalsIgnoreCase(e.getDistrictCode())) {
                                    position = idx;
                                }
                            }
                        }
                        sp_district_code.setSelection(position);
                    }

                    @Override
                    public void onFailure(Call<DistrictResponse> call, Throwable t) {
                        //Log.i(TAG, "Failed load District");
                        Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Distict ");

                    }
                });

            }


        } else {

            Call<DistrictResponse> calldistrict = services.getAllDistict();
            calldistrict.enqueue(new Callback<DistrictResponse>() {
                @Override
                public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                    List<client.android.psks.model.district.Entities> list = Arrays.asList(response.body().getEntities());
                    ArrayAdapter<client.android.psks.model.district.Entities> adapter =
                            new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
                    sp_district_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getDistrictCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.district.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getDistrictCode().equalsIgnoreCase(e.getDistrictCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_district_code.setSelection(position);
                }

                @Override
                public void onFailure(Call<DistrictResponse> call, Throwable t) {
                    //Log.i(TAG, "Failed load District");
                    Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " District ");

                }
            });

        }


    }

    private void setProvinceCode(final PKSMemberResponse res) {
        Call<ProvinceResponse> callProv = services.getAllProvince();
        callProv.enqueue(new Callback<ProvinceResponse>() {
            @Override
            public void onResponse(Call<ProvinceResponse> call, Response<ProvinceResponse> response) {
                client.android.psks.model.province.Entities[] list = response.body().getEntities();
                if (list != null) {
                    ProvinceAdapter adapter =
                            new ProvinceAdapter(getContext(), android.R.layout.simple_spinner_item, list);
                    sp_province_code.setAdapter(adapter);

                    int position = 0;
                    if (res.getEntity().getProvinceCode() != null) {

                        for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                            client.android.psks.model.province.Entities e = response.body().getEntities()[idx];
                            if (res.getEntity().getProvinceCode().equalsIgnoreCase(e.getProvinceCode())) {
                                position = idx;
                            }
                        }
                    }
                    sp_province_code.setSelection(position);
                }

            }

            @Override
            public void onFailure(Call<ProvinceResponse> call, Throwable t) {
                //Log.i(TAG, "Failed Load Province");
                Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Provinsi ");
            }
        });


    }

    private void setParticipantType(final PKSMemberResponse res) {
        //ParameterGroupResponse resPart = FakeReferences.getParticipantType();

        Call<ParameterGroupResponse> callPart = services.getParticipantTypeList("PARTICIPANT_TYPE");
        callPart.enqueue(new Callback<ParameterGroupResponse>() {
            @Override
            public void onResponse(Call<ParameterGroupResponse> call, Response<ParameterGroupResponse> response) {
                Entities[] list = response.body().getEntities();
                SharedPrefsUtils.saveObjectToSharedPreference(getActivity(), PSKSConstanta.PARTICIPANT_TYPE, response.body());
//                ParameterGroupAdapter adapter =
//                        new ParameterGroupAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, list);
//                sp_participant_type.setAdapter(adapter);
                sp_participant_type.setItems(list);


                int position = 0;
                if (res.getEntity().getParticipantType() != null) {

                    for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                        Entities e = response.body().getEntities()[idx];
                        if (res.getEntity().getParticipantType().equalsIgnoreCase(e.getmParameterGroupValuePK().getValueCode())) {
                            position = idx;
                        }
                    }
                }
                sp_participant_type.setSelectedIndex(position);
            }

            @Override
            public void onFailure(Call<ParameterGroupResponse> call, Throwable t) {
                Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Tipe Participan ");
            }
        });


    }

    private void setPsksType(final PKSMemberResponse res) {
        //ParameterGroupResponse resPsks = FakeReferences.getPSKSTYPEType();
        String participan = sp_participant_type.getText().toString().length() == 0 ? "1" : sp_participant_type.getText().toString();
        String kodeParticipan = getParticipanType(getActivity(), participan);
        String param = "PART$" + kodeParticipan;
        Call<ParameterGroupResponse> callPsks = services.getPsksType(param);
        callPsks.enqueue(new Callback<ParameterGroupResponse>() {
            @Override
            public void onResponse(Call<ParameterGroupResponse> call, Response<ParameterGroupResponse> response) {
                Entities[] list = response.body().getEntities();
//                ParameterGroupAdapter adapter =
//                        new ParameterGroupAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, list);
//                sp_pkstype.setAdapter(adapter);
                sp_pkstype.setItems(list);
                if (list.length == 1) {
                    sp_pkstype.setItems(list[0].getDescription(), "");
                }


                int position = 0;
                if (res.getEntity().getPsksType() != null) {

                    for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                        Entities e = response.body().getEntities()[idx];
                        if (res.getEntity().getPsksType().equalsIgnoreCase(e.getmParameterGroupValuePK().getValueCode())) {
                            position = idx;
                        }
                    }
                }
                sp_pkstype.setSelectedIndex(position);
            }

            @Override
            public void onFailure(Call<ParameterGroupResponse> call, Throwable t) {
                Utils.showSnakbarMessage(coordinatorLayout, MSG_FAILED + " Tipe PSKS ");
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<String> mPaths = null;
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            img_photo.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
            img_photo.setTag(R.id.value, mPaths.get(0));

        }
    }

    private StringBuilder createImageStr(String filePath) {
        StringBuilder builder = new StringBuilder();
        File file = new File(filePath);
        builder.append("");
        String extension = "";
        int i = file.getName().lastIndexOf('.');
        if (i > 0) {
            extension = file.getName().substring(i + 1);
        }

        //builder.append(Utils.encodeFileToBase64Binary(filePath));
        try {
            builder.append(URLEncoder.encode(extension));
            builder.append(";");
            builder.append(URLEncoder.encode(file.getName()));
            builder.append(";");
            builder.append(file.length());
            builder.append(";");
            builder.append("image/");
            builder.append(URLEncoder.encode(extension));
            builder.append(";");
            builder.append(Utils.encodeFileToBase64Binary(filePath));
            Log.i(TAG, "FILE :::: " + URLEncoder.encode(Utils.encodeFileToBase64Binary(filePath), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return builder;
    }

    private StringBuilder createFromExistinct() {
        StringBuilder builder = new StringBuilder();
        builder.append("");
        String extension = "";
        int i = imageResponse.getOriginalFileName().lastIndexOf('.');
        if (i > 0) {
            extension = imageResponse.getOriginalFileName().substring(i + 1);
        }
        builder.append(extension);
        builder.append(";");
        builder.append(imageResponse.getOriginalFileName());
        builder.append(";");
        builder.append(imageResponse.getBinaryData().length());
        builder.append(";");
        builder.append(imageResponse.getContentType());
        builder.append(";");
        builder.append(imageResponse.getBinaryData());
        builder.append("");
        return builder;
    }


    @Override
    public void doBack() {
        MenuItem item = ((MainActivity) getActivity())
                .navigationView.getMenu().getItem(0);
        item.setChecked(true);
        ((MainActivity) getActivity()).onNavigationItemSelected(item);

    }

    private String getParticipanType(Context c, String desc) {
        String result = "1";
        ParameterGroupResponse response = SharedPrefsUtils
                .getSavedObjectFromPreference(c,
                        PSKSConstanta.PARTICIPANT_TYPE, ParameterGroupResponse.class);

        if (response != null) {
            for (Entities en : response.getEntities()) {
                if (en.getDescription().equalsIgnoreCase(desc)) {
                    result = en.getmParameterGroupValuePK().getValueCode();
                }
            }
        }


        return result;
    }


}
