package client.android.psks.service;

import client.android.psks.model.response.AccessToken;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by winzaldi on 11/16/17.
 */

public interface PSKSTokenService {

    String GRANT_TYPE = "implicit";


    @GET("oauth/token")
    Call<AccessToken> getAccessToken(@Query("grant_type") String grantType,
                                     @Query("client_id") String clientId,
                                     @Query("client_secret") String clientSecret,
                                     @Query("username") String username,
                                     @Query("password") String password);
}
