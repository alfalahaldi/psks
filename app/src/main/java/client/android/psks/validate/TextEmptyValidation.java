package client.android.psks.validate;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import client.android.psks.R;

/**
 * Created by winzaldi on 11/19/17.
 */

public class TextEmptyValidation implements TextWatcher {

    private TextView textView;
    private Context mContext;
    public TextEmptyValidation(TextView textView, Context context) {
        this.textView = textView;
        this.mContext = context;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(textView.getText().toString().trim().length()==0){
            textView.setError( mContext.getResources().getString(R.string.empty_text));
        }else {
            textView.setError(null);
        }

    }
}
