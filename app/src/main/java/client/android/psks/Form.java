package client.android.psks;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.InputType;
import android.text.Spanned;
import android.text.SpannedString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.whygraphics.multilineradiogroup.MultiLineRadioGroup;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import client.android.psks.model.district.DistrictAdapter;
import client.android.psks.model.district.DistrictResponse;
import client.android.psks.model.province.ProvinceAdapter;
import client.android.psks.model.province.ProvinceResponse;
import client.android.psks.model.question.QuestionDetailAdapter;
import client.android.psks.model.question.QuestionDetailResponse;
import client.android.psks.model.quitioner.Entities;
import client.android.psks.model.quitioner.QuestionnaireResponse;
import client.android.psks.model.response.BaseResponse;
import client.android.psks.model.subdistrict.SubDistrictResponse;
import client.android.psks.model.subdistrict.SubdistrictAdapter;
import client.android.psks.model.village.VillageAdapter;
import client.android.psks.model.village.VillageResponse;
import client.android.psks.service.PSKSServices;
import client.android.psks.service.ServiceGenerator;
import client.android.psks.utils.PSKSConstanta;
import client.android.psks.utils.SharedPrefsUtils;
import client.android.psks.utils.Utils;
import client.android.psks.validate.TextEmptyValidation;
import client.android.psks.validate.TextEmptyValidationOnFocus;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Form extends AppCompatActivity {

    private static final String TAG = Form.class.getName();
    QuestionnaireResponse response;
    private LinearLayout llLayout;
    private LayoutInflater inflater;
    private List<View> container = new ArrayList<>();
    private EditText selectedEditText;
    private EditText selectedEditTextImgVideo;
    private EditText selectedEditTextFile;
    private boolean FL_BUKTI = false;
    private Calendar calendar;
    private ImagePicker imagePicker;
    private SearchableSpinner spinnerProvince, spinnerDistrict, spinnerSubDistrict, spinnerVillage;
    private VideoPicker videoPicker;
    private ImageView selectedImageView;
    private ImageView selectedVideo;
    private ProgressBar progressBar;
    private ScrollView mScrollView;
    private String surveyAssigmentId;
    private PSKSServices services = null;
    private TimePickerDialog mTimePicker;
    private DatePickerDialog mDatePickerDialog;
    public static final String SCROLL_POSITION = "position_scroll";
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verifyStoragePermissions(this);
        setContentView(R.layout.activity_form);
        llLayout = findViewById(R.id.llLayout);
        progressBar = findViewById(R.id.progressBar);
        inflater = getLayoutInflater();
        calendar = Calendar.getInstance();
        mScrollView = findViewById(R.id.mainScrollView);

        if (!Utils.isNetworkConnected(getApplicationContext())) {
            Toast.makeText(Form.this, "Internet Not Connected", Toast.LENGTH_SHORT).show();
            return;
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle.getString(PSKSConstanta.SURVEY_ASSIGMENT_ID) != null) {
            surveyAssigmentId = bundle.getString(PSKSConstanta.SURVEY_ASSIGMENT_ID);
        }
//        surveyAssigmentId = "60001";
        //      String token = "3ca5ef65-ca62-4c45-b33d-4d1420d4e035";
        String token = SharedPrefsUtils.getFromPrefs(this, PSKSConstanta.NEKOT_APP, "");
        services = ServiceGenerator.createService(PSKSServices.class, this,
                token);
        Log.i(TAG, surveyAssigmentId);
        progressBar.setVisibility(View.VISIBLE);
        Call<QuestionnaireResponse> callQue = services.getQuestionnaireQuestion(surveyAssigmentId);
        callQue.enqueue(new Callback<QuestionnaireResponse>() {
            @Override
            public void onResponse(final Call<QuestionnaireResponse> call, Response<QuestionnaireResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null && response.body().getEntities().length > 0) {


                    // set tanggal input laporan
                    TextInputLayout textInputLayoutTglLaporan = (TextInputLayout) inflater.inflate(R.layout.comp_date, null);
                    final TextInputEditText inputEditTextTglLpaoran = (TextInputEditText) textInputLayoutTglLaporan.getEditText();
                    inputEditTextTglLpaoran.setTag("-99");
                    inputEditTextTglLpaoran.setTag(R.id.date, "DATE");
                    textInputLayoutTglLaporan.setHint("Tanggal Input Laporan");
                    inputEditTextTglLpaoran.setTag(R.id.validate, "true");
                    inputEditTextTglLpaoran.setInputType(InputType.TYPE_NULL);
                    Utils.hideKeyboardFrom(Form.this, inputEditTextTglLpaoran);
                    inputEditTextTglLpaoran.addTextChangedListener(new TextEmptyValidation(inputEditTextTglLpaoran, getApplicationContext()));
                    inputEditTextTglLpaoran.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) pickDate(inputEditTextTglLpaoran, Form.this);

                        }
                    });
                    inputEditTextTglLpaoran.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pickDate(inputEditTextTglLpaoran, Form.this);
                        }
                    });

                    llLayout.addView(textInputLayoutTglLaporan);
                    container.add(inputEditTextTglLpaoran);
                    //

                    Log.i(TAG, response.body().toString());

                    Entities[] entities = response.body().getEntities();
                    //Entities[] entities = FakeUtils.getQuestioner(FakeUtils.FORM_JSON_2).getEntities();
                    for (int i = 0; i < entities.length; i++) {
                        Entities entity = entities[i];


                        if (entity.getQuestionType().equalsIgnoreCase("SHORT_TEXT")) {
                            final TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_short_text, null);
                            final TextInputEditText textInputEditText = (TextInputEditText) textInputLayout.getEditText();
                            textInputLayout.setHint(entity.getQuestionName());
                            setQuestionRequireTag(textInputEditText, entity);
                            textInputEditText.setId(Utils.generateViewId());
                            //set variable name
                            textInputEditText.setTag(entity.getQuestionnaireQuestionId());
                            if (entity.getRequired().equalsIgnoreCase("true")) {
                                textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText, getApplicationContext()));
                            }


                            //add child view to parent
                            llLayout.addView(textInputLayout);
                            container.add(textInputEditText);
                        } else if (entity.getQuestionType().equalsIgnoreCase("PARAGRAPH")) {

                            final TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_paragraph, null);
                            textInputLayout.setHint(entity.getQuestionName());
                            final TextInputEditText textInputEditText = (TextInputEditText) textInputLayout.getEditText();
                            setQuestionRequireTag(textInputEditText, entity);
                            textInputEditText.setId(Utils.generateViewId());
                            //set variable name
                            textInputEditText.setTag(entity.getQuestionnaireQuestionId());
                            if (entity.getRequired().equalsIgnoreCase("true")) {
                                textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText, getApplicationContext()));

                            }

                            llLayout.addView(textInputLayout);
                            container.add(textInputEditText);
                        } else if (entity.getQuestionType().equalsIgnoreCase("DROPDOWN")) {
                            LinearLayout llradio = (LinearLayout) inflater.inflate(R.layout.comp_label_spinner, null);
                            TextView textView = (TextView) llradio.getChildAt(0);
                            textView.setText(entity.getQuestionName());
                            llLayout.addView(llradio);
                            llradio.setTag(entity.getQuestionnaireQuestionId());
                            final MaterialSpinner spinner = (MaterialSpinner) inflater.inflate(R.layout.comp_spinner, null);
                            setQuestionRequireTag(spinner, entity);
                            spinner.setHint(entity.getQuestionName());
                            //get Data For Spinner
                            //QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.QDETAIL);

                            Call<QuestionDetailResponse> callDetail = services.getQuestionnaireQuestionDtl(entity.getQuestionnaireQuestionId());
                            progressBar.setVisibility(View.VISIBLE);
                            callDetail.enqueue(new Callback<QuestionDetailResponse>() {
                                @Override
                                public void onResponse(Call<QuestionDetailResponse> call, Response<QuestionDetailResponse> response) {
                                    progressBar.setVisibility(View.GONE);
                                    QuestionDetailAdapter dataAdapter = new QuestionDetailAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                    spinner.setItems(Arrays.asList(response.body().getEntities()));

                                }

                                @Override
                                public void onFailure(Call<QuestionDetailResponse> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(Form.this, "Failed Load Data Detail ", Toast.LENGTH_SHORT).show();
                                }
                            });

                            spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                    client.android.psks.model.question.Entities curr = (client.android.psks.model.question.Entities) item;
                                    spinner.setSelectedIndex(position);
                                    List<client.android.psks.model.question.Entities> list = spinner.getItems();
                                    for (int ien = 0; ien < list.size(); ien++) {
                                        client.android.psks.model.question.Entities en = list.get(ien);
                                        if (en.getMQuestionnaireQuestionDtl().equalsIgnoreCase(curr.getMQuestionnaireQuestionDtl())) {
                                            spinner.setSelectedIndex(ien);
                                        }
                                    }
                                }
                            });


                            llLayout.addView(spinner);
                            container.add(spinner);
                        } else if (entity.getQuestionType().equalsIgnoreCase("CHECKBOX")) {
                            final LinearLayout llcheckbox = (LinearLayout) inflater.inflate(R.layout.comp_label_checkbox, null);
                            llcheckbox.setFocusable(true);
                            llcheckbox.setFocusableInTouchMode(true);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(0, 24, 0, 16);
                            llcheckbox.setLayoutParams(layoutParams);
                            TextView textView = (TextView) llcheckbox.getChildAt(0);
                            textView.setText(entity.getQuestionName());
                            setQuestionRequireTag(llcheckbox, entity);

                            Call<QuestionDetailResponse> callDetail = services.getQuestionnaireQuestionDtl(entity.getQuestionnaireQuestionId());
                            progressBar.setVisibility(View.VISIBLE);
                            callDetail.enqueue(new Callback<QuestionDetailResponse>() {
                                @Override
                                public void onResponse(Call<QuestionDetailResponse> call, Response<QuestionDetailResponse> response) {
                                    progressBar.setVisibility(View.GONE);
                                    for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                        client.android.psks.model.question.Entities item = response.body().getEntities()[idx];
                                        CheckBox checkBox = (CheckBox) inflater.inflate(R.layout.comp_checkbox, null);
                                        checkBox.setFocusable(true);
                                        checkBox.setFocusableInTouchMode(true);
                                        checkBox.setTag(item.getMQuestionnaireQuestionDtl());
                                        checkBox.setText(item.getLabelText());
                                        llcheckbox.addView(checkBox);
                                    }
                                }

                                @Override
                                public void onFailure(Call<QuestionDetailResponse> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(Form.this, "Failed Load Data Detail ", Toast.LENGTH_SHORT).show();
                                }
                            });
                            llLayout.addView(llcheckbox);
                            container.add(llcheckbox);

                        } else if (entity.getQuestionType().equalsIgnoreCase("CHOICE")) {
//

                            LinearLayout lradio = (LinearLayout) inflater.inflate(R.layout.comp_label_radio, null);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(0, 16, 0, 0);
                            lradio.setLayoutParams(layoutParams);
                            TextView textView = (TextView) lradio.getChildAt(0);
                            textView.setText(entity.getQuestionName());
                            llLayout.addView(lradio);
                            final RadioGroup radioGroup = (RadioGroup) lradio.getChildAt(1);
                            setQuestionRequireTag(radioGroup, entity);

                            //QuestionDetailResponse detailResponse = FakeUtils.getQuestionDetail(FakeUtils.JNS_KEL_JSON);
                            Call<QuestionDetailResponse> callDetail = services.getQuestionnaireQuestionDtl(entity.getQuestionnaireQuestionId());
                            progressBar.setVisibility(View.VISIBLE);
                            callDetail.enqueue(new Callback<QuestionDetailResponse>() {
                                @Override
                                public void onResponse(Call<QuestionDetailResponse> call, Response<QuestionDetailResponse> response) {
                                    progressBar.setVisibility(View.GONE);
                                    if (response.body().getEntities().length > 2) {
                                        radioGroup.setOrientation(LinearLayout.VERTICAL);
                                    } else {
                                        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
                                    }
                                    for (int idx = 0; idx < response.body().getEntities().length; idx++) {
                                        client.android.psks.model.question.Entities item = response.body().getEntities()[idx];
                                        RadioButton radioButton = new RadioButton(Form.this);
                                        radioButton.setTag(item.getMQuestionnaireQuestionDtl());
                                        radioButton.setId(Utils.generateViewId());
                                        radioButton.setText(item.getLabelText());
                                        radioGroup.addView(radioButton, idx);
                                    }
                                    if (radioGroup.getChildAt(0) != null)
                                        ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);

                                }

                                @Override
                                public void onFailure(Call<QuestionDetailResponse> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(Form.this, "Failed Load Data Detail ", Toast.LENGTH_SHORT).show();
                                }
                            });

                            container.add(radioGroup);
                        } else if (entity.getQuestionType().equalsIgnoreCase("SECTION")) {
                            Log.i(TAG, "SECTION");
                            LinearLayout llsection = (LinearLayout) inflater.inflate(R.layout.comp_section, null);
                            llLayout.addView(llsection);
                        } else if (entity.getQuestionType().equalsIgnoreCase("DATE")) {
                            TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_date, null);
                            textInputLayout.setHint(entity.getQuestionName());
                            final TextInputEditText inputEditText = (TextInputEditText) textInputLayout.getEditText();
                            inputEditText.setTag(entity.getQuestionnaireQuestionId());
                            inputEditText.setTag(R.id.date, "DATE");
                            setQuestionRequireTag(inputEditText, entity);
                            inputEditText.setInputType(InputType.TYPE_NULL);

                            inputEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) pickDate(inputEditText, Form.this);
                                }
                            });
                            inputEditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    pickDate(inputEditText, Form.this);
                                }
                            });
                            llLayout.addView(textInputLayout);
                            container.add(inputEditText);

                        } else if (entity.getQuestionType().equalsIgnoreCase("TIME")) {
                            Log.i(TAG, "TIME");
                            TextInputLayout textInputLayout = (TextInputLayout) inflater.inflate(R.layout.comp_time, null);
                            textInputLayout.setHint(entity.getQuestionName());
                            final TextInputEditText inputEditText = (TextInputEditText) textInputLayout.getEditText();

                            inputEditText.setInputType(InputType.TYPE_NULL);
                            setQuestionRequireTag(inputEditText, entity);

                            inputEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) pickTime(inputEditText, Form.this);
                                }
                            });
                            inputEditText.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    pickTime(inputEditText, Form.this);
                                }
                            });
                            llLayout.addView(textInputLayout);
                            container.add(inputEditText);
                        } else if (entity.getQuestionType().equalsIgnoreCase("SLIDER")) {
                            Log.i(TAG, "RATING");
                            LinearLayout llRatingBar = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_start, null);
                            RatingBar ratingBar = (RatingBar) llRatingBar.getChildAt(0);
                            setQuestionRequireTag(ratingBar, entity);
                            Log.i(TAG, "RATING" + entity.getLinearToNum());
                            ratingBar.setNumStars(Integer.parseInt(entity.getLinearToNum()));
                            ratingBar.setRating(Integer.parseInt(entity.getLinearStartNum()));

                            llLayout.addView(llRatingBar);
                            container.add(ratingBar);


                        } else if (entity.getQuestionType().equalsIgnoreCase("LINEAR")) {
                            Log.i(TAG, "LINEAR");
                            int count = Integer.parseInt(entity.getLinearToNum());
                            if (count > 5) {
                                LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_multiline_radiogroup_linear, null);
                                TextView textView = (TextView) linearLayout.getChildAt(0);
                                textView.setText(entity.getQuestionName());
                                LinearLayout linearLayout2 = (LinearLayout) linearLayout.getChildAt(1);
                                MultiLineRadioGroup radioGroup = (MultiLineRadioGroup) linearLayout2.getChildAt(1);
                                setQuestionRequireTag(radioGroup, entity);
                                for (int idx = 0; idx < Integer.parseInt(entity.getLinearToNum()); idx++) {
                                    RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.comp_radio_text_ontop, null);
                                    int number = idx + 1;
                                    radioButton.setText(number + "");
                                    radioGroup.addView(radioButton);
                                }

                                llLayout.addView(linearLayout);
                                container.add(radioGroup);
                            } else {
                                LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_radiogroup_linear, null);
                                TextView textView = (TextView) linearLayout.getChildAt(0);
                                textView.setText(entity.getQuestionName());
                                LinearLayout linearLayout2 = (LinearLayout) linearLayout.getChildAt(1);
                                RadioGroup radioGroup = (RadioGroup) linearLayout2.getChildAt(1);
                                setQuestionRequireTag(radioGroup, entity);
                                for (int idx = 0; idx < Integer.parseInt(entity.getLinearToNum()); idx++) {
                                    RadioButton radioButton = (RadioButton) inflater.inflate(R.layout.comp_radio_text_ontop, null);
                                    int number = idx + 1;
                                    radioButton.setText(number + "");
                                    radioGroup.addView(radioButton);
                                }
                                if (radioGroup.getChildAt(0) != null) {
                                    ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
                                }

                                llLayout.addView(linearLayout);
                                container.add(radioGroup);
                            }

                        } else if (entity.getQuestionType().equalsIgnoreCase("IMAGE")) {
                            final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_upload, null);
                            final TextView label = (TextView) linearLayout.getChildAt(0);
                            label.setText(entity.getQuestionName());
                            final ImageView imageView = (ImageView) linearLayout.getChildAt(1);
                            imageView.setImageResource(R.drawable.ic_picture);
                            setQuestionRequireTag(imageView, entity);
                            imageView.setTag(R.id.image, "IMAGE");
                            Button button = (Button) linearLayout.getChildAt(2);
                            button.setText(getString(R.string.chose_photo_file));
                            selectedImageView = imageView;

                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    imagePicker = new ImagePicker.Builder(Form.this)
                                            .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                                            .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                                            .directory(ImagePicker.Directory.DEFAULT)
                                            .extension(ImagePicker.Extension.PNG)
                                            .extension(ImagePicker.Extension.JPG)
                                            .scale(600, 600)
                                            .allowMultipleImages(false)
                                            .enableDebuggingMode(true)
                                            .build();


                                    FL_BUKTI = false;
                                }
                            });
                            container.add(imageView);
                            llLayout.addView(linearLayout);

                        } else if (entity.getQuestionType().equalsIgnoreCase("VIDEO")) {
                            final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_upload, null);
                            final TextView label = (TextView) linearLayout.getChildAt(0);
                            label.setText(entity.getQuestionName());
                            final ImageView imageView = (ImageView) linearLayout.getChildAt(1);
                            imageView.setImageResource(R.drawable.ic_youtube);
                            imageView.setTag(R.id.video, "VIDEO");
                            setQuestionRequireTag(imageView, entity);
                            Button button = (Button) linearLayout.getChildAt(2);
                            button.setText(getString(R.string.chose_video_file));
                            selectedVideo = imageView;

                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    videoPicker = new VideoPicker.Builder(Form.this)
                                            .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
                                            .directory(VideoPicker.Directory.DEFAULT)
                                            .extension(VideoPicker.Extension.MP4)
                                            .enableDebuggingMode(true)
                                            .build();
                                    FL_BUKTI = false;

                                }
                            });
                            container.add(imageView);
                            llLayout.addView(linearLayout);


                        } else if (entity.getQuestionType().equalsIgnoreCase("REFERENCE")) {

                            LinearLayout llradio = (LinearLayout) inflater.inflate(R.layout.comp_label_spinner, null);
                            TextView textView = (TextView) llradio.getChildAt(0);
                            textView.setText(entity.getQuestionName());
                            llLayout.addView(llradio);
                            llradio.setTag(entity.getQuestionnaireQuestionId());

                            if (entity.getReferenceValueType().equalsIgnoreCase("PROVINCE")) {
                                final LinearLayout llspinner = (LinearLayout) inflater.inflate(R.layout.comp_dropdown_search, null);
                                final SearchableSpinner spinner = (SearchableSpinner) llspinner.getChildAt(0);
                                setQuestionRequireTag(spinner, entity);
                                progressBar.setVisibility(View.VISIBLE);
                                Call<ProvinceResponse> callProv = services.getAllProvince();
                                callProv.enqueue(new Callback<ProvinceResponse>() {
                                    @Override
                                    public void onResponse(Call<ProvinceResponse> call, Response<ProvinceResponse> response) {
                                        progressBar.setVisibility(View.GONE);
                                        ProvinceAdapter adapter =
                                                new ProvinceAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                        spinner.setAdapter(adapter);
                                    }

                                    @Override
                                    public void onFailure(Call<ProvinceResponse> call, Throwable t) {
                                        progressBar.setVisibility(View.GONE);
                                        Log.i(TAG, "Failed Load Province");
                                    }
                                });

                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (spinnerDistrict != null) {
                                            client.android.psks.model.province.Entities entity = (client.android.psks.model.province.Entities) spinner.getSelectedItem();
                                            progressBar.setVisibility(View.VISIBLE);
                                            Call<DistrictResponse> calldistrict = services.getDistictByProvince(entity.getProvinceCode());
                                            calldistrict.enqueue(new Callback<DistrictResponse>() {
                                                @Override
                                                public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                                                    progressBar.setVisibility(View.GONE);
                                                    DistrictAdapter adapter =
                                                            new DistrictAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                                    spinnerDistrict.setAdapter(adapter);

                                                }

                                                @Override
                                                public void onFailure(Call<DistrictResponse> call, Throwable t) {
                                                    progressBar.setVisibility(View.GONE);
                                                    Log.i(TAG, "Failed load District");

                                                }
                                            });
                                        }

                                        //Toast.makeText(Form.this, entity.getProvinceCode() + ":"+ entity.getProvinceName(), Toast.LENGTH_SHORT).show();

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                llLayout.addView(llspinner);
                                container.add(spinner);


                            } else if (entity.getReferenceValueType().equalsIgnoreCase("DISTRICT")) {
                                //get Data For Spinner
//
                                //createSpinner and set Adapater
                                final LinearLayout llspinner = (LinearLayout) inflater.inflate(R.layout.comp_dropdown_search, null);
                                final SearchableSpinner spinner = (SearchableSpinner) llspinner.getChildAt(0);
                                setQuestionRequireTag(spinner, entity);
                                progressBar.setVisibility(View.VISIBLE);
                                Call<DistrictResponse> calldistrict = services.getAllDistict();
                                calldistrict.enqueue(new Callback<DistrictResponse>() {
                                    @Override
                                    public void onResponse(Call<DistrictResponse> call, Response<DistrictResponse> response) {
                                        progressBar.setVisibility(View.GONE);
                                        DistrictAdapter adapter =
                                                new DistrictAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                        spinner.setAdapter(adapter);

                                    }

                                    @Override
                                    public void onFailure(Call<DistrictResponse> call, Throwable t) {
                                        progressBar.setVisibility(View.GONE);
                                        Log.i(TAG, "Failed load District");

                                    }
                                });
                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (spinnerSubDistrict != null) {
                                            client.android.psks.model.district.Entities entity = (client.android.psks.model.district.Entities) spinner.getSelectedItem();
                                            progressBar.setVisibility(View.VISIBLE);
                                            Call<SubDistrictResponse> calldistrict = services.getSubDistictByDistrictCode(entity.getDistrictCode());
                                            calldistrict.enqueue(new Callback<SubDistrictResponse>() {
                                                @Override
                                                public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                                                    progressBar.setVisibility(View.GONE);
                                                    SubdistrictAdapter adapter =
                                                            new SubdistrictAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                                    spinnerSubDistrict.setAdapter(adapter);

                                                }

                                                @Override
                                                public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                                                    progressBar.setVisibility(View.GONE);
                                                    Log.i(TAG, "Failed load Sub District");

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
//
                                spinnerDistrict = spinner;
                                llLayout.addView(llspinner);
                                container.add(spinner);


                            } else if (entity.getReferenceValueType().equalsIgnoreCase("SUBDISTRICT")) {
                                //get Data For Spinner
                                //createSpinner and set Adapater
                                final LinearLayout llspinner = (LinearLayout) inflater.inflate(R.layout.comp_dropdown_search, null);
                                final SearchableSpinner spinner = (SearchableSpinner) llspinner.getChildAt(0);
                                setQuestionRequireTag(spinner, entity);
                                progressBar.setVisibility(View.VISIBLE);
                                Call<SubDistrictResponse> callsubdistrict = services.getSubDistrict();
                                callsubdistrict.enqueue(new Callback<SubDistrictResponse>() {
                                    @Override
                                    public void onResponse(Call<SubDistrictResponse> call, Response<SubDistrictResponse> response) {
                                        progressBar.setVisibility(View.GONE);
                                        SubdistrictAdapter adapter =
                                                new SubdistrictAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                        spinner.setAdapter(adapter);

                                    }

                                    @Override
                                    public void onFailure(Call<SubDistrictResponse> call, Throwable t) {
                                        progressBar.setVisibility(View.GONE);
                                        Log.i(TAG, "Failed load SubDistrict");

                                    }
                                });
                                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        if (spinnerVillage != null) {
                                            client.android.psks.model.subdistrict.Entities entity = (client.android.psks.model.subdistrict.Entities) spinner.getSelectedItem();
                                            progressBar.setVisibility(View.VISIBLE);
                                            Call<VillageResponse> calldistrict = services.getVillageBySubDistrictCode(entity.getSubDistrictCode());
                                            calldistrict.enqueue(new Callback<VillageResponse>() {
                                                @Override
                                                public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {
                                                    progressBar.setVisibility(View.GONE);
                                                    VillageAdapter adapter =
                                                            new VillageAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                                    spinnerVillage.setAdapter(adapter);

                                                }

                                                @Override
                                                public void onFailure(Call<VillageResponse> call, Throwable t) {
                                                    progressBar.setVisibility(View.GONE);
                                                    Log.i(TAG, "Failed load Village");

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                                spinnerSubDistrict = spinner;
                                llLayout.addView(llspinner);
                                container.add(spinner);

                            } else if (entity.getReferenceValueType().equalsIgnoreCase("VILLAGE")) {
                                //get Data For Spinner
                                final LinearLayout llspinner = (LinearLayout) inflater.inflate(R.layout.comp_dropdown_search, null);
                                final SearchableSpinner spinner = (SearchableSpinner) llspinner.getChildAt(0);
                                setQuestionRequireTag(spinner, entity);
                                Call<VillageResponse> callProv = services.getAllVillage();
                                callProv.enqueue(new Callback<VillageResponse>() {
                                    @Override
                                    public void onResponse(Call<VillageResponse> call, Response<VillageResponse> response) {
                                        VillageAdapter adapter =
                                                new VillageAdapter(Form.this, android.R.layout.simple_spinner_item, response.body().getEntities());
                                        spinner.setAdapter(adapter);
                                    }

                                    @Override
                                    public void onFailure(Call<VillageResponse> call, Throwable t) {
                                        Log.i(TAG, "Failed Load Village");
                                    }
                                });


                                spinnerVillage = spinner;
                                llLayout.addView(llspinner);
                                container.add(spinner);

                            }


                        } else if (entity.getQuestionType().equalsIgnoreCase("CANVAS")) {
                            final LinearLayout llcanvas = (LinearLayout) inflater.inflate(R.layout.comp_ttd, null);
                            final TextView tv_ttd = (TextView) llcanvas.getChildAt(0);
                            tv_ttd.setText(entity.getQuestionName());
                            final SignaturePad signature = (SignaturePad) llcanvas.getChildAt(1);
                            setQuestionRequireTag(signature,entity);
                            llLayout.addView(llcanvas);
                            container.add(signature);
                        }


                    }

                    //Tambahan Untuk File Apapun

                    final LinearLayout linearLayoutAdd = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_upload_file, null);
                    final TextInputLayout textInputLayoutAdd = (TextInputLayout) linearLayoutAdd.getChildAt(0);
                    final EditText textInputEditTextAdd = textInputLayoutAdd.getEditText();
                    textInputEditTextAdd.setTag("-97");
                    textInputEditTextAdd.setTag(R.id.image, "IMAGE");
                    textInputEditTextAdd.setTag(R.id.validate, "true");
                    textInputEditTextAdd.addTextChangedListener(new TextEmptyValidation(textInputEditTextAdd, getApplicationContext()));
                    textInputEditTextAdd .setOnFocusChangeListener(new TextEmptyValidationOnFocus(textInputEditTextAdd));

                    final LinearLayout linearLayoutButtonAdd = (LinearLayout) linearLayoutAdd.getChildAt(1);
                    Button buttonImgAdd = (Button) linearLayoutButtonAdd.getChildAt(0);
                    //Button buttonVideo = (Button) linearLayoutButton.getChildAt(1);

                    selectedEditTextFile = textInputEditTextAdd;

                    buttonImgAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            imagePicker = new ImagePicker.Builder(Form.this)
//                                    .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
//                                    .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
//                                    .directory(ImagePicker.Directory.DEFAULT)
//                                    .extension(ImagePicker.Extension.PNG)
//                                    .scale(600, 600)
//                                    .allowMultipleImages(false)
//                                    .enableDebuggingMode(true)
//                                    .build();
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("*/*");
                            startActivityForResult(intent,PSKSConstanta.REQUEST_PICK_FILE);
                            //FL_BUKTI = true;

                        }

                    });

                    container.add(textInputEditTextAdd);
                    llLayout.addView(linearLayoutAdd);

                    //add Image for Upload Bukti Gambar

                    final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.comp_upload_evident, null);
                    final TextInputLayout textInputLayoutBukti = (TextInputLayout) linearLayout.getChildAt(0);
                    final EditText textInputEditText = textInputLayoutBukti.getEditText();
                    textInputEditText.setTag("-98");
                    textInputEditText.setTag(R.id.image, "IMAGE");
                    textInputEditText.setTag(R.id.validate, "true");
                    textInputEditText.addTextChangedListener(new TextEmptyValidation(textInputEditText, getApplicationContext()));
                    textInputEditText.setOnFocusChangeListener(new TextEmptyValidationOnFocus(textInputEditText));

                    final LinearLayout linearLayoutButton = (LinearLayout) linearLayout.getChildAt(1);
                    Button buttonImg = (Button) linearLayoutButton.getChildAt(0);
                    //Button buttonVideo = (Button) linearLayoutButton.getChildAt(1);

                    selectedEditTextImgVideo = textInputEditText;

                    buttonImg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            imagePicker = new ImagePicker.Builder(Form.this)
                                    .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                                    .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                                    .directory(ImagePicker.Directory.DEFAULT)
                                    .extension(ImagePicker.Extension.PNG)
                                    .scale(600, 600)
                                    .allowMultipleImages(false)
                                    .enableDebuggingMode(true)
                                    .build();
//                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                            intent.setType("*/*");
//                            startActivityForResult(intent,PSKSConstanta.REQUEST_PICK_FILE);
                            FL_BUKTI = true;

                        }

                    });



//                    buttonVideo.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            videoPicker = new VideoPicker.Builder(Form.this)
//                                    .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
//                                    .directory(VideoPicker.Directory.DEFAULT)
//                                    .extension(VideoPicker.Extension.MP4)
//                                    .enableDebuggingMode(true)
//                                    .build();
//
//                        }
//                    });

                    container.add(textInputEditText);
                    llLayout.addView(linearLayout);

                    Button button = (Button) inflater.inflate(R.layout.comp_button, null);
                    button.setText("Submit");
                    llLayout.addView(button);
                    final HashMap<String, RequestBody> mapSubmit = new HashMap<>();
                    final List<MultipartBody.Part> mapfiles = new ArrayList<>();

                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //validasi
                            if (!checkValidation(container, Form.this)) {
                                return;
                            }

                            for (View item : container) {
                                //Log.i(TAG, item.getTag().toString());
                                if (item instanceof TextInputEditText) {
                                    TextInputEditText text = (TextInputEditText) item;
                                    Log.i(TAG, text.getTag().toString());
                                    Log.i(TAG, text.getText().toString());
                                    //RequestBody value = ServiceGenerator.createPartFromString(text.getText().toString());
                                    //check if its date
                                    if (text.getTag(R.id.date) != null) {
                                        Log.i(TAG, " Date :" + text.getTag(R.id.date).toString());
                                        Log.i(TAG, " Text :" + text.getText().toString());

                                        String strValue = Utils.dateToString(Utils.stringToDate(text.getText().toString(),
                                                PSKSConstanta.DF_DD_MM_YYYY), PSKSConstanta.DF_YYYY_MM_DD);
                                        Log.i(TAG, " strValue :" + strValue);
                                        RequestBody value = ServiceGenerator.createPartFromString(strValue);
                                        mapSubmit.put(text.getTag().toString(), value);

                                    } else if (text.getTag(R.id.image) != null) {
                                        Log.i(TAG, text.getTag(R.id.image).toString());
                                        Log.i(TAG, text.getText().toString());
                                        Log.i(TAG, " FileXXX :" + text.getTag(R.id.image).toString());
                                        Log.i(TAG, " FileXXX :" + text.getText().toString());

//                                        RequestBody value = ServiceGenerator.createRequestPartFile
//                                                (text.getTag().toString(),text.getText().toString());
//                                        mapSubmit.put(text.getTag().toString(), ServiceGenerator.createRequestPartFile(text.getTag().toString(),text.getText().toString()));

                                        mapfiles.add(ServiceGenerator.createPartFile(PSKSConstanta.MEDIA_IMAGE, text.getTag().toString(),
                                                text.getText().toString()));
                                    } else if (text.getTag(R.id.video) != null) {
                                        Log.i(TAG, text.getTag(R.id.video).toString());
                                        Log.i(TAG, text.getText().toString());
                                        Log.i(TAG, " FileXXX :" + text.getTag(R.id.file).toString());
                                        Log.i(TAG, " FileXXX :" + text.getText().toString());

//                                        RequestBody value = ServiceGenerator.createRequestPartFile
//                                                (text.getTag().toString(),text.getText().toString());
//                                        mapSubmit.put(text.getTag().toString(), ServiceGenerator.createRequestPartFile(text.getTag().toString(),text.getText().toString()));

                                        mapfiles.add(ServiceGenerator.createPartFile(PSKSConstanta.MEDIA_VIDEO, text.getTag().toString(),
                                                text.getText().toString()));
                                    } else {
                                        RequestBody value = ServiceGenerator.createPartFromString(text.getText().toString());
                                        mapSubmit.put(text.getTag().toString(), value);
                                    }


                                } else if (item instanceof SignaturePad) {
                                    SignaturePad signature = (SignaturePad) item;
                                    Log.i(TAG, signature.getTag().toString());
                                    String filename = String.format("signature_%d.jpg", System.currentTimeMillis());
                                    File photo = new File(getAlbumStorageDir("psks"), filename);
                                    try {
                                        saveBitmapToJPG(signature.getSignatureBitmap(), photo);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    mapfiles.add(ServiceGenerator.createPartFile(PSKSConstanta.MEDIA_IMAGE, signature.getTag().toString(),
                                            photo.getAbsolutePath()));

                                } else if (item instanceof Spinner) {
                                    Spinner spinner = (Spinner) item;
                                    Log.i(TAG, spinner.getSelectedItem().toString());
                                    Log.i(TAG, spinner.getTag().toString());
                                    RequestBody value = ServiceGenerator.createPartFromString(spinner.getSelectedItem().toString());
                                    mapSubmit.put(spinner.getTag().toString(), value);
                                } else if (item instanceof MaterialSpinner) {
                                    MaterialSpinner spinner = (MaterialSpinner) item;
                                    Log.i(TAG, spinner.getTag().toString());
                                    Log.i(TAG, "Material :" + spinner.getText());
                                    RequestBody value = null;
                                    if (null != spinner.getItems()) {
                                        Object obj = spinner.getItems().get(spinner.getSelectedIndex());
                                        if (obj instanceof client.android.psks.model.question.Entities) {
                                            client.android.psks.model.question.Entities en = (client.android.psks.model.question.Entities) obj;
                                            value = ServiceGenerator.createPartFromString(en.getMQuestionnaireQuestionDtl());
                                        }
                                        //Log.i(TAG, en.getMQuestionnaireQuestionDtl());
                                        //value = ServiceGenerator.createPartFromString(en.getMQuestionnaireQuestionDtl());
                                        mapSubmit.put(spinner.getTag().toString(), value);
                                    }

                                } else if (item instanceof SearchableSpinner) {
                                    SearchableSpinner spinner = (SearchableSpinner) item;
                                    RequestBody value = null;
                                    if (null != spinner.getSelectedItem()) {
                                        Object obj = spinner.getSelectedItem();
                                        if (obj instanceof client.android.psks.model.province.Entities) {
                                            client.android.psks.model.province.Entities en = (client.android.psks.model.province.Entities) obj;
                                            value = ServiceGenerator.createPartFromString(en.getProvinceCode());
                                        } else if (obj instanceof client.android.psks.model.district.Entities) {
                                            client.android.psks.model.district.Entities en = (client.android.psks.model.district.Entities) obj;
                                            value = ServiceGenerator.createPartFromString(en.getDistrictCode());
                                        } else if (obj instanceof client.android.psks.model.subdistrict.Entities) {
                                            client.android.psks.model.subdistrict.Entities en = (client.android.psks.model.subdistrict.Entities) obj;
                                            value = ServiceGenerator.createPartFromString(en.getSubDistrictCode());
                                        } else if (obj instanceof client.android.psks.model.village.Entities) {
                                            client.android.psks.model.village.Entities en = (client.android.psks.model.village.Entities) obj;
                                            value = ServiceGenerator.createPartFromString(en.getVillageCode());
                                        }
                                        //Log.i(TAG, en.getMQuestionnaireQuestionDtl());
                                        //value = ServiceGenerator.createPartFromString(en.getMQuestionnaireQuestionDtl());
                                        mapSubmit.put(spinner.getTag().toString(), value);
                                    }


                                } else if (item instanceof RadioGroup) {
                                    RadioGroup radioGroup = (RadioGroup) item;
                                    Log.i(TAG, radioGroup.getTag() + "");
                                    int selectedId = radioGroup.getCheckedRadioButtonId();
                                    if (selectedId != -1) {
                                        RadioButton radioButton = radioGroup.findViewById(selectedId);
                                        Log.i(TAG, radioButton.getText().toString());
                                        String strValue = null == radioButton.getTag() ? radioButton.getText().toString() : radioButton.getTag().toString();
                                        RequestBody value = ServiceGenerator.createPartFromString(strValue);
                                        mapSubmit.put(radioGroup.getTag().toString(), value);
                                    }

                                } else if (item instanceof RatingBar) {
                                    RatingBar ratingBar = (RatingBar) item;
                                    Log.i(TAG, ratingBar.getNumStars() + "");
                                    Log.i(TAG, ratingBar.getTag().toString());
                                    RequestBody value = ServiceGenerator.createPartFromString(String.valueOf(ratingBar.getRating()));
                                    mapSubmit.put(ratingBar.getTag().toString(), value);
                                } else if (item instanceof ImageView) {
                                    ImageView imageView = (ImageView) item;
                                    Log.i(TAG, imageView.getTag() + "");
                                    Log.i(TAG, imageView.getTag(R.id.value) + "");

                                    if (imageView.getTag(R.id.value) != null) {
                                        mapfiles.add(ServiceGenerator.createPartFile(PSKSConstanta.MEDIA_IMAGE, imageView.getTag().toString(),
                                                imageView.getTag(R.id.value).toString()));
                                    }

                                } else if (item instanceof LinearLayout) {
                                    LinearLayout linearLayout = (LinearLayout) item;
                                    String strValue = "";
                                    for (int i = 0; i < linearLayout.getChildCount(); i++) {
                                        if (linearLayout.getChildAt(i) instanceof CheckBox) {
                                            CheckBox checkBox = (CheckBox) linearLayout.getChildAt(i);

                                            if (checkBox.isChecked()) {
                                                Log.i(TAG, checkBox.getText().toString());
                                                strValue = strValue + checkBox.getText().toString() + ",";
                                            }


                                        }
                                    }

                                    if (strValue.length() > 2) {
                                        Log.d(TAG, strValue.substring(0, strValue.lastIndexOf(",") - 1));
                                        RequestBody value = ServiceGenerator.createPartFromString(strValue.substring(0, strValue.lastIndexOf(",") - 1));
                                        mapSubmit.put(linearLayout.getTag().toString(), value);
                                    }

                                }

                            }
                            //send data to the server
                            //set assignment id
                            mapSubmit.put("surveyAssignmentId", ServiceGenerator.createPartFromString(surveyAssigmentId));
                            Call<BaseResponse> callSubmit = services.saveDataQuestionnaire(mapSubmit, mapfiles);
                            progressBar.setVisibility(View.VISIBLE);
                            callSubmit.enqueue(new Callback<BaseResponse>() {
                                @Override
                                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                    progressBar.setVisibility(View.GONE);

                                    if (response.body() != null) {
                                        if (response.body().getResponseCode().equalsIgnoreCase("200")) {
                                            Spanned message = new SpannedString("Sumissiion Success");
                                            Utils.showMessageDialog(message, "Submission", Form.this);
                                        } else {
                                            Log.e(TAG, response.body().getResponseMessage());
                                            Log.e(TAG, "XX" + response.body().getResponseMessage());
                                            Toast.makeText(Form.this, response.body().getResponseMessage(), Toast.LENGTH_SHORT).show();
                                        }

                                    } else {
                                        Log.e(TAG, "Message " + response.errorBody().toString());
                                        Toast.makeText(Form.this, "Failed to Submit data", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<BaseResponse> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(Form.this, "Failed to Submit data", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                } else {
                    Spanned message = new SpannedString("Tidak ada Form");
                    Utils.showMessageDialog(message, "Informasi", Form.this);
                    Log.e(TAG, message.toString());
                }


            }

            @Override
            public void onFailure(Call<QuestionnaireResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                //Log.i(TAG, "Failed to create Form");
                Spanned message = new SpannedString("Gagal buat form");
                Utils.showMessageDialog(message, "Informasi", Form.this);

            }
        });

        //response = FakeUtils.getQuestioner(FakeUtils.FORM_JSON_2);


    }

    private void setQuestionRequireTag(final View view, Entities entity) {
        view.setTag(entity.getQuestionnaireQuestionId());
        view.setTag(R.id.validate, entity.getRequired());
        view.setId(Utils.generateViewId());
        if (entity.getRequired().equalsIgnoreCase("true")) {
            if (view instanceof EditText) {
                final EditText text = (EditText) view;
                text.addTextChangedListener(new TextEmptyValidation(text, view.getContext()));
                text.setOnFocusChangeListener(new TextEmptyValidationOnFocus(text));
            }

        }
    }

    // Date picker
    public void pickDate(EditText et, Context context) {
        if (et != null) {
            selectedEditText = et;
            if (mDatePickerDialog != null) {
                mDatePickerDialog.show();
            } else {
                mDatePickerDialog = new DatePickerDialog(context, datePickerListener, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                mDatePickerDialog.show();
            }

        }
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat(PSKSConstanta.DF_DD_MM_YYYY);

            if (selectedEditText != null) {
                selectedEditText.setText(sdf.format(calendar.getTime()));
            }
        }

    };

    // Time picker
    public void pickTime(EditText et, Context context) {
        if (et != null) {
            selectedEditText = et;
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            if (mTimePicker != null) {
                mTimePicker.show();
            } else {
                mTimePicker = new TimePickerDialog(context, timePickerListener, hour, minute, true);//Yes 24 hour time
                mTimePicker.show();
            }

        }
    }


    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            calendar.set(Calendar.HOUR, selectedHour);
            calendar.set(Calendar.MINUTE, selectedMinute);

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

            if (selectedEditText != null) {
                selectedEditText.setText(sdf.format(calendar.getTime()));
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            List<String> mPaths = null;
        if (requestCode == PSKSConstanta.REQUEST_PICK_FILE && resultCode == RESULT_OK) {
            String PathHolder = getPath(getApplicationContext(),data.getData());
            Toast.makeText(getApplicationContext(), PathHolder, Toast.LENGTH_LONG).show();
            Log.i(TAG, "onActivityResult: " + PathHolder);
            selectedEditTextFile.setText(PathHolder);
        }
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH);
            //Your Code
            if (FL_BUKTI) {
                selectedEditTextImgVideo.setText(mPaths.get(0));
            } else {
                selectedImageView.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
                selectedImageView.setTag(R.id.value, mPaths.get(0));
                Log.i(TAG, selectedImageView.getTag() + "");
            }


        } else if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            mPaths = (List<String>) data.getSerializableExtra(VideoPicker.EXTRA_VIDEO_PATH);
            Log.i(TAG, mPaths.get(0));
            if (FL_BUKTI) {
                selectedEditTextImgVideo.setText(mPaths.get(0));
            } else {
                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mPaths.get(0), MediaStore.Video.Thumbnails.MINI_KIND);
                selectedVideo.setImageBitmap(thumb);
                selectedVideo.setTag(R.id.value, mPaths.get(0));
                Log.i(TAG, selectedVideo.getTag() + "");
            }


        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
        outState.putIntArray(SCROLL_POSITION,
                new int[]{mScrollView.getScrollX(), mScrollView.getScrollY()});
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int[] position = savedInstanceState.getIntArray(SCROLL_POSITION);
        if (position != null)
            mScrollView.post(new Runnable() {
                public void run() {
                    mScrollView.scrollTo(position[0], position[1]);
                }
            });
    }

    private boolean checkValidation(List<View> container, Context context) {
        int i = 0;

        for (View v : container) {
            i++;
            Log.i(TAG, "Validate :: " + v.getTag().toString());
            Log.i(TAG, "Validate :: " + i);
            String required = v.getTag(R.id.validate) != null ? v.getTag(R.id.validate).toString() : "false";
            if (required.equalsIgnoreCase("true")) {
                if (v instanceof EditText) {
                    EditText text = (EditText) v;
                    if (text.getText().toString().trim().length() == 0) {
                        text.requestFocus();
                        text.setError(v.getContext().getResources().getString(R.string.empty_text));
                        Toast.makeText(this, context.getResources().getString(R.string.empty_text), Toast.LENGTH_LONG).show();
                        return false;
                    }
                } else if (v instanceof SignaturePad) {
                    SignaturePad signature = (SignaturePad) v;
                    if (signature.getTag().toString().equalsIgnoreCase("true") && signature.isEmpty()) {
                        Toast.makeText(this, context.getResources().getString(R.string.empty_signature), Toast.LENGTH_LONG).show();
                        signature.requestFocus();
                        return false;
                    }

                } else if (v instanceof MultiLineRadioGroup) {
                    MultiLineRadioGroup radioGroup = (MultiLineRadioGroup) v;
                    int id = radioGroup.getCheckedRadioButtonId();

                    if (id == -1) {
                        radioGroup.requestFocus();
                        Toast.makeText(this, context.getResources().getString(R.string.empty_check), Toast.LENGTH_LONG).show();
                        return false;
                    }
                } else if (v instanceof ImageView) {
                    ImageView imageView = (ImageView) v;
                    if (imageView.getTag(R.id.value) == null) {
                        Toast.makeText(this, context.getResources().getString(R.string.empty_pic_vid), Toast.LENGTH_LONG).show();
                        imageView.requestFocus();
                        return false;
                    }
                } else if (v instanceof LinearLayout && !(v instanceof RadioGroup)) {
                    LinearLayout linearLayout = (LinearLayout) v;
                    int count = linearLayout.getChildCount();
                    boolean checked = false;

                    for (int idx = 0; idx < count; idx++) {
                        View view = linearLayout.getChildAt(idx);

                        if (view instanceof AppCompatCheckBox) {
                            CheckBox checkBox = (CheckBox) view;
                            if (checkBox.isChecked()) {
                                checked = checkBox.isChecked();
                            }

                        }
                    }
                    if (!checked) {
                        Toast.makeText(this, context.getResources().getString(R.string.empty_check), Toast.LENGTH_LONG).show();
                        linearLayout.requestFocus();
                        return false;
                    }

                }
            }
        }
        return true;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(Form.this, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(TAG, "Folder psks gagal dibuat");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        Form.this.sendBroadcast(mediaScanIntent);
    }



    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


}
